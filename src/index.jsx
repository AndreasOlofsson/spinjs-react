import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Spinner } from 'spin.js';

export default class ReactSpinner extends Component {
    static propTypes = {
        config: PropTypes.object,
        style: PropTypes.object
    };

    static defaultProps = {
        config: {},
        style: null
    };

    componentDidMount() {
        const { config } = this.props;
        const spinConfig = {
            position: 'relative',
            width: 2,
            radius: 10,
            length: 7,
            color: 'black',
            ...config,
        };

        this.spinner = new Spinner(spinConfig);
        this.spinner.spin(this.container);
    }

    componentWillUnmount() {
        this.spinner.stop();
    }

    render() {
        return <span ref={ ref => ref && (this.container = ref) }
                     style={ this.props.style } />;
    }
}
